package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static java.time.Duration.*;

public class SamplePageTest {
    WebDriver driver;

    @BeforeClass
    public void setup() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions().setHeadless(true);

        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(ofSeconds(10));
        driver.get("https://demo.realworld.io/#/");
    }

    @AfterClass
    public void teardown() {
        driver.quit();
    }

    @Test
    void testIfWebPageWasOpened() {
        String expectedPhrase = "Conduit";
        String actualTitle = driver.getTitle();

        Assert.assertTrue(actualTitle.contains(expectedPhrase), String.format("The actual web page title: '%s', doesn't contain an expected phrase: '%s'", actualTitle, expectedPhrase));
    }}
